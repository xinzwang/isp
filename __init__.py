"""
    Title

    author: wxz
    date: 
    github: https://github.com/xinzwang
"""

import sys

from pathlib import Path

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # ISP root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
