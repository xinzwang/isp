"""
    black level correction

    author: wxz
    date: 2021-12-2
    github: https://github.com/xinzwang
"""

import numpy as np


def black_level_correction(img, black_level=512, white_level=16383):
    data = (img - black_level) / (white_level - black_level)

    # clipping within range(0,1)
    data = np.clip(data, 0, 1)

    return data
