"""
    Title

    author: wxz
    date: 2021-12-5
    github: https://github.com/xinzwang
"""

import numpy as np


def gamma_correction(img, multiplier=80.0, gamma=2.2):
    # luma adjustment
    data = np.log10(multiplier) * img
    data = np.clip(data, 0, 1)

    # gamma by value
    data = data ** (1.0 / gamma)

    # gamma by table
    # TODO: look up table for gamma

    # gamma by equation
    # data = a * np.exp(b * data) + data + a * data - a * np.exp(b) * data - a

    data = np.clip(data, 0, 1)

    return data
