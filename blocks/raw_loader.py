"""
    Title

    author: wxz
    date: 
    github: https://github.com/xinzwang
"""

import rawpy
import numpy as np


def load_from_path(path):
    # read from file
    raw = rawpy.imread(path)

    # without margin
    raw_img = raw.raw_image_visible.astype(np.float32)

    # get rgb image
    rgb_img = raw.postprocess(use_camera_wb=True, half_size=False, no_auto_bright=True, output_bps=16)
    rgb_img = rgb_img / 65535

    # calculate shape and bit_depth
    shape = raw_img.shape
    bit_depth = np.ceil(np.log2(np.max(raw_img)))

    return raw_img, rgb_img, shape, bit_depth
