"""
    Title

    author: wxz
    date: 2021-12-3
    github: https://github.com/xinzwang
"""

import numpy as np
from isp.blocks.utils import *


def tone_curve(img, strength_multiplier=0.1, kernel_size=(3, 3), sigma=1.0):
    # convert to gray image
    gray_img = 0.299 * img[:, :, 0] + 0.587 * img[:, :, 1] + 0.114 * img[:, :, 2]

    kernel_size = np.round(kernel_size)  # round to int
    kernel_size[0] = max(0, kernel_size[0])
    kernel_size[1] = max(0, kernel_size[1])
    kernel = gauss_kernel(kernel_size=kernel_size, sigma=sigma)

    mask = conv2d(gray_img, kernel)
    mask = strength_multiplier * mask

    # calculate the alpha image
    t = np.power(0.5, mask)
    alpha = np.zeros_like(img, dtype=np.float32)
    alpha[:, :, 0] = t
    alpha[:, :, 1] = t
    alpha[:, :, 2] = t

    # output
    out = np.zeros_like(img, dtype=np.float32)
    out = np.power(img, alpha)
    out = np.clip(out, 0, 1)

    return out
