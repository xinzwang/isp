"""
    Title

    author: wxz
    date: 
    github: https://github.com/xinzwang
"""

import sys

sys.path.append("")

from IspHelper import IspHelper

params = {
        'black_level_correction': {
            'black_level': 512,
            'white_level': 16383,
        },
        'white_balance': {
            'channel_gain': (1.9296875, 1.0, 2.26171875)
        },
        'color_correction': {
            'color_matrix': [[.9020, -.2890, -.0715],
                             [-.4535, 1.2436, .2348],
                             [-.0934, .1919, .7086]]
        },
        'gamma_correction': {
            'multiplier': 80.0,
            'gamma': 2.2
        },
        'tone_curve': {
            'strength_multiplier': 1,
            'kernel_size': (5, 5),
            'sigma': 1.0
        }
    }

if __name__ == '__main__':
    helper = IspHelper()
    helper.set_params(params)

    helper.load_raw('../datas/00001_00_10s.ARW')

    helper.forward()

    helper.imshow()
