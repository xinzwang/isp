"""
    Title

    author: wxz
    date: 
    github: https://github.com/xinzwang
"""

import numpy as np
from scipy import signal

a = np.asarray([[1, 1, 1],
                [2, 2, 2],
                [3, 3, 3],
                [4, 4, 4]])

print('垂直纹理:\n', signal.convolve2d(a, [[1], [0], [-1]], mode='same', boundary='symm'))
print('水平纹理:\n', signal.convolve2d(a, [[1, 0, -1]], mode='same', boundary='symm'))
