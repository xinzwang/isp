"""
    Title

    author: wxz
    date: 
    github: https://github.com/xinzwang
"""

import sys
import rawpy
from matplotlib import pyplot as plt

LOD_path = "G:/datasets/LOD/" if sys.platform == 'win32' else "/home/wxz/codes/datasets/LOD/"

img_path = LOD_path + 'images/raw/1.CR2'

raw = rawpy.imread(img_path)

# demosaik = rawpy.DemosaicAlgorithm

rgb = raw.postprocess(exp_shift=0.25)

plt.title('postprocess(exp_shift=0.25)')
plt.imshow(rgb)
plt.savefig('runs/postprocess(exp_shift=0.25).jpg', dpi=300)


# rgb = raw.postprocess()
# rgb = raw.postprocess(use_camera_wb=False, use_auto_wb=False)
# rgb = raw.postprocess(use_camera_wb=False, use_auto_wb=True)

# 使用
# rgb = raw.postprocess(use_auto_wb=True)
# rgb = raw.postprocess(gamma=(30.0, 40.0))
# rgb = raw.postprocess(exp_shift=4)                  # 0.25 - 8
# rgb = raw.postprocess(exp_preserve_highlights=0.8)  # 0.0-1.0
# rgb = raw.postprocess(user_sat=3)

# 不使用
# rgb = raw.postprocess(dcb_iterations=4)
# rgb = raw.postprocess(chromatic_aberration=(0.5,3))





# plt.subplot(2, 3, 1)
# plt.title('default')
# plt.imshow(rgb_default)
#
# plt.subplot(2, 3, 2)
# plt.title('rgb_1')
# plt.imshow(rgb_1)
#
# plt.subplot(2, 3, 3)
# plt.title('rgb_2')
# plt.imshow(rgb_2)
#
# plt.subplot(2, 3, 4)
# plt.title('rgb_3')
# plt.imshow(rgb_3)
#
# plt.subplot(2, 3, 5)
# plt.title('rgb_4')
# plt.imshow(rgb_4)
#
# plt.savefig('runs/rawpy.jpg', dpi=600)
